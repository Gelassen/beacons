package com.example.android.takethewallet.client.ui;

import android.app.Fragment;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.takethewallet.BluetoothManager;
import com.example.android.takethewallet.R;
import com.example.android.takethewallet.client.Storage;

public class DeviceFragment extends Fragment implements DeviceAdapter.ClickListener{

    private static final String WALLET_FLAG = "WALLET";

    private TextView device;
    private TextView deviceAddress;
    private RecyclerView recyclerView;
    private DeviceAdapter adapter;

    private BluetoothManager bluetoothManager;
    private BluetoothBroadcastReceiver bluetoothReceiver;

    public static DeviceFragment getInstance(final boolean walletBeacon) {
        DeviceFragment frag = new DeviceFragment();
        Bundle data = new Bundle();
        data.putBoolean(WALLET_FLAG, walletBeacon);
        frag.setArguments(data);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_beacon, container, false);
        device = (TextView) view.findViewById(R.id.device);
        deviceAddress = (TextView) view.findViewById(R.id.deviceAddress);

        recyclerView = (RecyclerView) view.findViewById(R.id.featured_collection);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new DeviceAdapter();
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);

        bluetoothManager = new BluetoothManager(getActivity());
        bluetoothReceiver = new BluetoothBroadcastReceiver();
        bluetoothManager.registerReceiver(getActivity(), bluetoothReceiver);

        bluetoothManager.startDiscovery();

        // initialize
        final boolean isWallet = getArguments().getBoolean(WALLET_FLAG);
        BluetoothDevice bluetoothDevice = isWallet ?
                Storage.getWallet(getActivity(), bluetoothManager)
                    : Storage.getCar(getActivity(), bluetoothManager);
        device.setText(bluetoothDevice == null ? getString(R.string.device_placeholder) : bluetoothDevice.getName());
        deviceAddress.setText(bluetoothDevice == null ? getString(R.string.device_placeholder) : bluetoothDevice.getAddress());

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bluetoothManager.unregisterReceiver(getActivity(), bluetoothReceiver);
    }

    @Override
    public void onClick(Device device) {
        this.device.setText(device.getName());
        this.deviceAddress.setText(device.getAddress());
        boolean isWallet = getArguments().getBoolean(WALLET_FLAG);
        if (isWallet) {
            Storage.saveWallet(getActivity(), bluetoothManager.lookup(device.getAddress()));
        } else {
            Storage.saveCar(getActivity(), bluetoothManager.lookup(device.getAddress()));
        }

    }

    private class BluetoothBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                adapter.addDevice(new Device(
                                device.getName(),
                                device.getAddress()
                        )
                );

            }
        }
    }
}
