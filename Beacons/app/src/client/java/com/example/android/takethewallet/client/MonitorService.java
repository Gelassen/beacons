package com.example.android.takethewallet.client;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Pair;

import com.example.android.takethewallet.BluetoothManager;
import com.example.android.takethewallet.Params;
import com.example.android.takethewallet.R;

public class MonitorService extends Service {

    public static void startMonitor(Context context) {
        Intent intent = new Intent(context, MonitorService.class);
        context.startService(intent);
    }

    public static void stopMonitor(Context context) {
        Intent intent = new Intent(context, MonitorService.class);
        context.stopService(intent);
    }

    private BluetoothManager bluetoothManager;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        bluetoothManager = new BluetoothManager(this);
        bluetoothManager.registerReceiver(this, receiver);
        // schedule discovery
        final Handler observe = new Handler();
        observe.post(new Runnable() {
            @Override
            public void run() {
                bluetoothManager.startDiscovery();
                observe.postDelayed(this, getResources().getInteger(R.integer.observe_delay));
            }
        });

        // setup notification
        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_content))
                .setOngoing(true)
                .setContentIntent(
                        PendingIntent.getActivity(
                                this, 0,
                                new Intent(this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP),
                                PendingIntent.FLAG_UPDATE_CURRENT
                        ));
        startForeground(Params.Notification.NOTIFICATION_MONITOR, builder.build());
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bluetoothManager.unregisterReceiver(this, receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        private final int TIME_THRESHOLD = 60 * 1000;

        private Pair<BluetoothDevice, Long> carBeacon;
        private Pair<BluetoothDevice, Long> walletBeacon;

        private int carBeaconRssi = Short.MIN_VALUE;
        private int walletBeaconRssi = Short.MIN_VALUE;

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                final int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                BluetoothDevice carDevice = Storage.getCar(getApplicationContext(), bluetoothManager);
                BluetoothDevice walletDevice = Storage.getWallet(getApplicationContext(), bluetoothManager);
                if (device.getAddress().equals(carDevice.getAddress())) {
                    carBeacon = new Pair<>(carDevice, System.currentTimeMillis());
                    carBeaconRssi = rssi;
                } else if (device.getAddress().equals(walletDevice.getAddress())) {
                    walletBeacon = new Pair<>(walletDevice, System.currentTimeMillis());
                    walletBeaconRssi = rssi;
                }
                notifyIfYouInCarWithoutWallet();
            }
        }

        private void notifyIfYouInCarWithoutWallet() {
            if (carBeacon == null) return;
            if ((walletIsOutOfRange() && beaconWithMe(carBeaconRssi))
                    || (walletWithMe() && !beaconWithMe(walletBeaconRssi))) {
                // notify
                Notification.Builder builder = new Notification.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.notification_wallet_title))
                        .setContentText(getString(R.string.notification_wallet_content))
                        .setContentIntent(
                                PendingIntent.getActivity(
                                        getApplicationContext(), 0,
                                        new Intent(getApplicationContext(), MainActivity.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP),
                                        PendingIntent.FLAG_UPDATE_CURRENT
                                ));
                NotificationManager notificationManager = (NotificationManager)
                        getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(Params.Notification.NOTIFICATION_MONITOR, builder.build());
            }
        }

        private boolean walletWithMe() {
            return walletBeacon != null;
        }

        private boolean walletIsOutOfRange() {
            return (walletBeacon == null || walletBeacon.second >= TIME_THRESHOLD)
                    && carBeacon.second >= TIME_THRESHOLD;
        }

        private boolean beaconWithMe(int rssi) {
            final int threshold = getResources().getInteger(R.integer.rssi_threshold);
            return  rssi >= threshold;
        }

    };
}
