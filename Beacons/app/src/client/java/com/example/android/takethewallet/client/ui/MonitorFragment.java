package com.example.android.takethewallet.client.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.example.android.takethewallet.R;
import com.example.android.takethewallet.client.MonitorService;
import com.example.android.takethewallet.client.Storage;

public class MonitorFragment extends Fragment {

    public static MonitorFragment getInstance() {
        MonitorFragment frag = new MonitorFragment();
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_monitor, container, false);
        ToggleButton switcher = (ToggleButton) view.findViewById(R.id.switcher);
        switcher.setChecked(Storage.isMonitored(getActivity()));
        switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MonitorService.startMonitor(getActivity());
                } else {
                    MonitorService.stopMonitor(getActivity());
                }
                Storage.setMonitored(getActivity(), isChecked);
            }
        });
        return view;
    }
}
