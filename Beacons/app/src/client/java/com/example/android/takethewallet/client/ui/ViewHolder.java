package com.example.android.takethewallet.client.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.android.takethewallet.R;

public class ViewHolder extends RecyclerView.ViewHolder {
    private TextView view;
    private DeviceAdapter.ClickListener listener;

    public ViewHolder(View itemView, DeviceAdapter.ClickListener listener) {
        super(itemView);
        view = (TextView) itemView.findViewById(R.id.device);
        this.listener = listener;
    }

    public void bind(final Device device) {
        view.setText(device.getName());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(device);
                }
            }
        });
    }
}
