package com.example.android.takethewallet.client;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.android.takethewallet.BluetoothManager;

public class Storage {

    private static final String NAMESPACE = Storage.class.getName();
    private static final String STORAGE = NAMESPACE.concat(".STORAGE");

    private static final String KEY_WALLET_NAME = NAMESPACE.concat(".WALLET_NAME");
    private static final String KEY_WALLET_ADDRESS = NAMESPACE.concat(".WALLET_ADDRESS");
    private static final String KEY_CAR_NAME = NAMESPACE.concat(".CAR_NAME");
    private static final String KEY_CAR_ADDRESS = NAMESPACE.concat(".CAR_ADDRESS");

    private static final String KEY_IS_MONITORED = NAMESPACE.concat(".IS_MONITORED");

    public static void saveWallet(Context context, BluetoothDevice device) {
        SharedPreferences pref = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        pref.edit()
                .putString(KEY_WALLET_NAME, device.getName())
                .putString(KEY_WALLET_ADDRESS, device.getAddress())
                .apply();
    }

    public static BluetoothDevice getWallet(Context context, BluetoothManager bluetoothManager) {
        SharedPreferences pref = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        final String address = pref.getString(KEY_WALLET_ADDRESS, "");
        return address.isEmpty() ? null : bluetoothManager.lookup(address);
    }

    public static void saveCar(Context context, BluetoothDevice device) {
        SharedPreferences pref = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        pref.edit()
                .putString(KEY_CAR_NAME, device.getName())
                .putString(KEY_CAR_ADDRESS, device.getAddress())
                .apply();
    }

    public static BluetoothDevice getCar(Context context, BluetoothManager bluetoothManager) {
        SharedPreferences pref = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        final String address = pref.getString(KEY_CAR_ADDRESS, "");
        return address.isEmpty() ? null : bluetoothManager.lookup(address);
    }

    public static void setMonitored(Context context, boolean isMonitor) {
        SharedPreferences pref = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        pref.edit()
                .putBoolean(KEY_IS_MONITORED, isMonitor)
                .apply();
    }

    public static boolean isMonitored(Context context) {
        SharedPreferences pref = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        return pref.getBoolean(KEY_IS_MONITORED, false);
    }
}
