package com.example.android.takethewallet.beacon;

import android.content.Context;
import android.content.SharedPreferences;

public class Storage {

    private static final String NAMESPACE = Storage.class.getName();
    private static final String STORAGE = NAMESPACE.concat(".STORAGE");

    private static final String KEY_IS_OBSERVED = NAMESPACE.concat(".IS_OBSERVED");


    public static void setObserved(Context context, boolean isMonitor) {
        SharedPreferences pref = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        pref.edit()
                .putBoolean(KEY_IS_OBSERVED, isMonitor)
                .apply();
    }

    public static boolean isObserved(Context context) {
        SharedPreferences pref = context.getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        return pref.getBoolean(KEY_IS_OBSERVED, false);
    }
}
