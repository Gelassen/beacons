package com.example.android.takethewallet.beacon;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.example.android.takethewallet.*;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);

        ToggleButton observable = (ToggleButton) findViewById(R.id.observable);
        observable.setChecked(Storage.isObserved(this));
        observable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Storage.setObserved(getApplicationContext(), isChecked);
                if (isChecked) {
                    ObservableService.startAdvertisement(getApplicationContext());
                } else {
                    ObservableService.stopAdvertisement(getApplicationContext());
                }
            }
        });
    }

}
