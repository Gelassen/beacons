package com.example.android.takethewallet.beacon;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import com.example.android.takethewallet.BluetoothManager;
import com.example.android.takethewallet.Params;
import com.example.android.takethewallet.R;


public class ObservableService extends Service {

    public static void startAdvertisement(Context context) {
        Intent intent = new Intent(context, ObservableService.class);
        context.startService(intent);
    }

    public static void stopAdvertisement(Context context) {
        Intent intent = new Intent(context, ObservableService.class);
        context.stopService(intent);
    }

    final static Handler advertiseBySchedule = new Handler();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        advertisePeriod = ObservableService.this.getResources().getInteger(R.integer.advertise_period);
        discoverablePeriod = getResources().getInteger(R.integer.discoverable_period);
        advertiseBySchedule.post(scheduler);
        // setup notification
        Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_content))
                .setOngoing(true)
                .setContentIntent(
                        PendingIntent.getActivity(
                                this, 0,
                                new Intent(this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP),
                                PendingIntent.FLAG_UPDATE_CURRENT
                        ));
        startForeground(Params.Notification.NOTIFICATION_MONITOR, builder.build());
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        advertiseBySchedule.removeCallbacks(scheduler);
    }

    private int advertisePeriod;
    private int discoverablePeriod;

    private Runnable scheduler = new Runnable() {

        @Override
        public void run() {
            BluetoothManager bluetoothManager = new BluetoothManager(getApplicationContext());
            bluetoothManager.makeDiscoverable(ObservableService.this, advertisePeriod);
            advertiseBySchedule.postDelayed(this, discoverablePeriod);
        }
    };
}
