package com.example.android.takethewallet;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


public class BluetoothManager {

    private BluetoothAdapter adapter;

    public BluetoothManager(Context context) {
        android.bluetooth.BluetoothManager manager = (android.bluetooth.BluetoothManager)
                context.getSystemService(Context.BLUETOOTH_SERVICE);
        adapter = manager.getAdapter();
    }

    /**
     * Initiate new activity if needed. The result will be return on activity's method with
     * appropriate {@param resultCode}
     * */
    public boolean isEnabled(Activity activity, final int resultCode) {
        boolean isEnabled = adapter.isEnabled();
        if (isEnabled) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, resultCode);
        }
        return isEnabled;
    }

    public void startDiscovery() {
        adapter.startDiscovery();
    }

    public void makeDiscoverable(Context context, long time) {
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, time);
        context.startActivity(intent);
    }

    public BluetoothDevice lookup(String address) {
        return adapter.getRemoteDevice(address);
    }

    public void registerReceiver(Context context, BroadcastReceiver receiver) {
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        context.registerReceiver(receiver, filter);
    }

    public void unregisterReceiver(Context context, BroadcastReceiver receiver) {
        context.unregisterReceiver(receiver);
    }

}
