package com.example.android.takethewallet;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public abstract class BaseActivity extends ActionBarActivity {


    protected void setUpMainFragment(Fragment frag) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, frag)
                .commit();
    }

}
