package com.example.android.takethewallet;

public class Params {

    private static final String NAMESPACE = Params.class.getName();

    public static class Intent {
        private Intent() {
            // hidden
        }
        public final static String EXTRA_COMMAND = NAMESPACE.concat(".EXTRA_COMMAND");
        public final static String EXTRA_RECEIVER = NAMESPACE.concat(".EXTRA_RECEIVER");
    }

    public static class Notification {
        private Notification() {}

        public final static int NOTIFICATION_MONITOR = 1;
        public final static int NOTIFICATION_WALLET_IS_OUT_OF_RANGE = 2;
    }

}
